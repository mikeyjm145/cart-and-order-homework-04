﻿using System;
using System.Data;
using System.Web.UI;

/// <summary>
/// Front-Back end code for the Order page
/// </summary>
/// 
/// <author>
/// Michael Morguarge
/// </author>
/// 
/// <version>
/// Spring 2015
/// </version>
public partial class Order : Page
{
    /// <summary>
    /// The selected product from the drop down list
    /// </summary>
    private Product _selectedProduct;

    /// <summary>
    /// Binds the data for the dropdown list of products for the Order page
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The drop down list is data binded to its source
    /// </postcondition>
    /// 
    /// <param name="sender">Not used</param>
    /// <param name="anEvent">Not used</param>
    protected void Page_Load(object sender, EventArgs anEvent)
    {
        if (!IsPostBack)
        {
            this.ddlProducts.DataBind();
        }

        this._selectedProduct = this.GetSelectedProduct();

        this.lblName.Text = this._selectedProduct.Name;
        this.lblShortDescription.Text = this._selectedProduct.ShortDescription;
        this.lblLongDescription.Text = this._selectedProduct.LongDescription;
        this.lblUnitPrice.Text = this._selectedProduct.UnitPrice.ToString("c") + " each";
        this.imgProduct.ImageUrl = "Images/Products/" + this._selectedProduct.ImageFile;
    }

    /// <summary>
    /// Gets the selected product
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The Order display has been updated
    /// </postcondition>
    /// 
    /// <return>
    /// Returns the selected product
    /// </return>
    private Product GetSelectedProduct()
    {
        var productsTable = (DataView) this.ddlProductsDataSource.Select(DataSourceSelectArguments.Empty);

        if (productsTable == null)
            return null;

        productsTable.RowFilter = string.Format("ProductID = '{0}'", this.ddlProducts.SelectedValue);
        var row = productsTable[0];

        var p = new Product
        {
            ProductId = row["ProductID"].ToString(),
            Name = row["Name"].ToString(),
            ShortDescription = row["ShortDescription"].ToString(),
            LongDescription = row["Longdescription"].ToString(),
            UnitPrice = (decimal) row["UnitPrice"],
            ImageFile = row["ImageFile"].ToString()
        };

        return p;
    }

    /// <summary>
    /// Adds an item to the cart on button click
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The cart item has been added to the cart
    /// </postcondition> 
    /// 
    /// <param name="sender">Not used</param>
    /// <param name="anEvent">Not used</param>
    protected void btnAdd_Click(object sender, EventArgs anEvent)
    {
        if (!Page.IsValid)
            return;

        var cart = CartItemList.GetCart();
        var cartItem = cart[this._selectedProduct.ProductId];

        if (cartItem == null)
            cart.AddItem(this._selectedProduct, Convert.ToInt32(this.txtQuantity.Text));
        else
            cartItem.AddQuantity(Convert.ToInt32(this.txtQuantity.Text));
    }
}
