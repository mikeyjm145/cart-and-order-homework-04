﻿
using System;
using System.Web.UI;

/// <summary>
/// Front-Back end code for the Cart page
/// </summary>
/// 
/// <author>
/// Michael Morguarge
/// </author>
/// 
/// <version>
/// Spring 2015
/// </version>
public partial class Cart : Page
{
    /// <summary>
    /// The users cart of items to purchase
    /// </summary>
    private CartItemList _cart;

    /// <summary>
    /// Loads the cart for the Cart page
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The cart has been loaded and displayed
    /// </postcondition>
    /// 
    /// <param name="sender">Not used</param>
    /// <param name="anEvent">Not used</param>
    protected void Page_Load(object sender, EventArgs anEvent)
    {
        this._cart = CartItemList.GetCart();

        if (!IsPostBack)
        {
            this.DisplayCart();
        }
    }

    /// <summary>
    /// Displays the cart
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The cart display has been updated
    /// </postcondition>
    private void DisplayCart()
    {
        this.lstCart.Items.Clear();

        for (var i = 0; i < this._cart.Count; i++)
        {
            this.lstCart.Items.Add(this._cart[i].Display());
        }
    }

    /// <summary>
    /// Removes an item from the cart on button click
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The cart item has been removed and the cart display is updated
    /// </postcondition> 
    /// 
    /// <param name="sender">Not used</param>
    /// <param name="anEvent">Not used</param>
    protected void btnRemove_Click(object sender, EventArgs anEvent)
    {
        if (this.lstCart.SelectedIndex == -1)
        {
            this.lblMessage.Text = "Please select an item to remove.";
            return;
        }

        this._cart.RemoveAt(this.lstCart.SelectedIndex);
        this.DisplayCart();
    }

    /// <summary>
    /// Removes all items from the cart on button click
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The cart items have been removed and the cart display is updated
    /// </postcondition> 
    /// 
    /// <param name="sender">Not used</param>
    /// <param name="anEvent">Not used</param>
    protected void btnEmpty_Click(object sender, EventArgs anEvent)
    {
        if (this._cart.Count <= 0)
        {
            return;
        }

        this._cart.Clear();
        this.lstCart.Items.Clear();
    }

    /// <summary>
    /// Checks out the user with the cart on button click
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The user has checked out and purchased their items
    /// </postcondition> 
    /// 
    /// <param name="sender">Not used</param>
    /// <param name="anEvent">Not used</param>
    protected void btnCheckOut_Click(object sender, EventArgs anEvent)
    {
        this.lblMessage.Text = "Sorry, that function has not been implemented yet.";
    }
}