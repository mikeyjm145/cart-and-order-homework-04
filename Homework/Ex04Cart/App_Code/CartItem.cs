using System.Diagnostics;

/// <summary>
/// Creates an cart item you can store in a cart item list
/// </summary>
/// 
/// <author>
/// Michael Morguarge
/// </author>
/// 
/// <version>
/// Spring 2015
/// </version>
public class CartItem
{
    /// <summary>
    /// The product to purchase
    /// </summary>
    private Product _product;

    /// <summary>
    /// The quantity of the product
    /// </summary>
    private int _quantity;

    /// <summary>
    /// Creates a empty cart item
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// None
    /// </postcondition>
    public CartItem() {}

    /// <summary>
    /// Converts a product into a cart item to be purchased later
    /// </summary>
    /// 
    /// <precondition>
    /// product != null && quantity > 0
    /// </precondition>
    /// 
    /// <postcondition>
    /// The product is converted into a cart item with the specified quantity.
    /// </postcondition>
    /// 
    /// <param name="product">
    /// The product to be converted to a cart item
    /// </param>
    /// 
    /// <param name="quantity">
    /// The quantity of the product
    /// </param>
    public CartItem(Product product, int quantity)
    {
        Trace.Assert(condition: product != null, message: "Error: The product does not exist.");
        Trace.Assert(condition: quantity > 0, message: "Error: The quantity less than or equal to zero.");

        this._product = product;
        this._quantity = quantity;
    }

    /// <summary>
    /// The product to be purchased
    /// </summary>
    /// 
    /// <value>
    /// The product to be purchased
    /// </value>
    public Product Product
    {
        get { return this._product; }
        set
        {
            Trace.Assert(condition: value != null, message: "Error: The product does not exist.");
            this._product = value;
        }
    }

    /// <summary>
    /// The quantity of this product
    /// </summary>
    /// 
    /// <value>
    /// The quantity of the product
    /// </value>
    public int Quantity {
        get { return this._quantity; }
        set
        {
            Trace.Assert(condition: value > 0, message: "Error: The quantity less than or equal to zero.");
            this._quantity = value;
        }
    }

    /// <summary>
    /// Increments the quantity of the product
    /// </summary>
    /// 
    /// <precondition>
    /// The quantity must be greater than zero
    /// </precondition>
    /// 
    /// <postcondition>
    /// The quantity will increase by the amount specified
    /// </postcondition>
    /// 
    /// <param name="quantity">
    /// The amount to increment the quantity
    /// </param>
    public void AddQuantity(int quantity)
    {
        Trace.Assert(condition: quantity > 0, message: "Error: The quantity less than or equal to zero.");
        this._quantity += quantity;
    }

    /// <summary>
    /// Creates a string that displays the characteristics of the product
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// None
    /// </postcondition>
    /// 
    /// <returns>
    /// The characteristics of a product
    /// </returns>
    public string Display()
    {
        var displayString =
            this._product.Name + " (" + this._quantity
            + " at " + this._product.UnitPrice.ToString("c") + " each)";

        return displayString;
    }
    
}