﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Web;

/// <summary>
/// Creates an cart item you can store in a cart item list
/// </summary>
/// 
/// <author>
/// Michael Morguarge
/// </author>
/// 
/// <version>
/// Spring 2015
/// </version>
public class CartItemList
{
    /// <summary>
    /// List of items in the cart
    /// </summary>
    private readonly List<CartItem> _cartItems;

    /// <summary>
    /// Creates a empty cart list
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// None
    /// </postcondition>
    public CartItemList()
    {
        this._cartItems = new List<CartItem>();
    }

    /// <summary>
    /// The total number of items in the cart
    /// </summary>
    /// 
    /// <value>
    /// The total number of items in the cart
    /// </value>
    public int Count {
        get
        {
            return this._cartItems.Count;
        }
    }

    /// <summary>
    /// Gets the cart item at the specified index
    /// </summary>
    /// 
    /// <value>
    /// The cart item at the specified index
    /// </value>
    public CartItem this[int index]
    {
        get
        {
            return this._cartItems[index];
        }
        set
        {
            Trace.Assert(condition: value != null, message: "Error: The product you are trying to set is null.");
            this._cartItems[index] = value;
        }
    }

    /// <summary>
    /// Gets the cart item with the specified id
    /// </summary>
    /// 
    /// <value>
    /// The cart item with the specified id
    /// </value>
    public CartItem this[string id]
    {
        get {
            foreach (var c in this._cartItems)
                if (c.Product.ProductId == id) return c;
            return null;
        }
    }

    /// <summary>
    /// Maintains the state of the Cart Item List
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The cart's state is stored
    /// </postcondition>
    /// 
    /// <return>
    /// The cart item list
    /// </return>
    public static CartItemList GetCart()
    {
        var cart = (CartItemList) HttpContext.Current.Session["Cart"];
        if (cart == null)
            HttpContext.Current.Session["Cart"] = new CartItemList();
        return (CartItemList) HttpContext.Current.Session["Cart"];
    }

    /// <summary>
    /// Adds an item to the cart item list
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The item is added to the cart item list and the size of the list increases by one
    /// </postcondition>
    public void AddItem(Product product, int quantity)
    {
        var newItem = new CartItem(product, quantity);
        this._cartItems.Add(newItem);
    }

    /// <summary>
    /// Removes an item from the cart item list
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The item is removed from the cart item list and the size of the list decreases by one
    /// </postcondition>
    public void RemoveAt(int index)
    {
        this._cartItems.RemoveAt(index);
    }

    /// <summary>
    /// Clears the cart item list
    /// </summary>
    /// 
    /// <precondition>
    /// None
    /// </precondition>
    /// 
    /// <postcondition>
    /// The item are removed from the cart item list and the size of the list returns to zero
    /// </postcondition>
    public void Clear()
    {
        this._cartItems.Clear();
    }
}