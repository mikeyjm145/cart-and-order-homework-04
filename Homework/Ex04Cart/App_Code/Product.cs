using System.Diagnostics;

/// <summary>
/// Creates an product object that holds specified information about a product
/// </summary>
/// 
/// <author>
/// Michael Morguarge
/// </author>
/// 
/// <version>
/// Spring 2015
/// </version>
public class Product
{
    /// <summary>
    /// The product id
    /// </summary>
    private string _productId;

    /// <summary>
    /// The product name
    /// </summary>
    private string _name;

    /// <summary>
    /// The product short description
    /// </summary>
    private string _shortDescription;

    /// <summary>
    /// The product long description
    /// </summary>
    private string _longDescription;

    /// <summary>
    /// The product unit price
    /// </summary>
    private decimal _unitPrice;

    /// <summary>
    /// The product image
    /// </summary>
    private string _imageFile;

    /// <summary>
    /// The product id
    /// </summary>
    /// 
    /// <value>
    /// The product id
    /// </value>
    public string ProductId {
        get { return this._productId; }
        set
        {
            Trace.Assert(condition: value != null, message: "Error: The product ID of the product you are trying to set is null.");
            this._productId = value;
        }
    }

    /// <summary>
    /// The product name
    /// </summary>
    /// 
    /// <value>
    /// The product name
    /// </value>
    public string Name
    {
        get { return this._name; }
        set
        {
            Trace.Assert(condition: value != null, message: "Error: The name of the product you are trying to set is null.");
            this._name = value;
        }
    }

    /// <summary>
    /// The product short description
    /// </summary>
    /// 
    /// <value>
    /// The product short description
    /// </value>
    public string ShortDescription {
        get { return this._shortDescription; }
        set
        {
            Trace.Assert(condition: value != null, message: "Error: The short description of the product you are trying to set is null.");
            this._shortDescription = value;
        }
    }

    /// <summary>
    /// The product long description
    /// </summary>
    /// 
    /// <value>
    /// The product long description
    /// </value>
    public string LongDescription
    {
        get { return this._longDescription; }
        set
        {
            Trace.Assert(condition: value != null, message: "Error: The long description of the product you are trying to set is null.");
            this._longDescription = value;
        }
    }

    /// <summary>
    /// The product unit price
    /// </summary>
    /// 
    /// <value>
    /// The product unit price
    /// </value>
    public decimal UnitPrice
    {
        get { return this._unitPrice; }
        set
        {
            Trace.Assert(condition: value > 0, message: "Error: The price of the product you are trying to set is less than or equal to 0.");
            this._unitPrice = value;
        }
    }

    /// <summary>
    /// The product image
    /// </summary>
    /// 
    /// <value>
    /// The product image
    /// </value>
    public string ImageFile
    {
        get { return this._imageFile; }
        set
        {
            Trace.Assert(condition: value != null, message: "Error: The image of the product you are trying to set is null.");
            this._imageFile = value;
        }
    }
}